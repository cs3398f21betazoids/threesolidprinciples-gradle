/* Jacob Christian worked on this

   1. The manager class uses single responsibility. The manager only manages the workers and indicates
      their current status.
   2. The manager class is open for extension, but closed for modifications. As of right now, the class is
      only worrying about the current implementations needed by Worker. More can be added, but (hopefully)
      nothing needs to change at creation time.
   3. Interface Segregation principle: Not applied to this class. Uses a Worker object that has interface
      calling, and calls it for that worker, but the class itself does not implement anything.

      Used an arraylist to work on more than 1 worker at a time.
      Allows adding workers, make them work, give them a break, and dismiss them for the day
      Manager itself does not work, eat, or take a break with the general populace of workers, so methods not
      implemented.
 */

package threesolid;

import java.util.ArrayList;

public class Manager {
    ArrayList<Worker> workers;

    public Manager() {}

    public void addWorker(Worker w) {
        workers.add(w);
    }

    public void manageWork() {
        System.out.println("Everyone get to work!");
        for (Worker worker : workers){
            worker.work();
        }
    }

    public void manageBreak() {
        System.out.println("Everyone get to rest!");
        for (Worker worker : workers) {
            worker.rest();
        }
    }

    public void sendHome() {
        System.out.println("Good job team, everyone go home!");
        for (Worker worker : workers) {
            workers.remove(worker);
        }
    }
}
