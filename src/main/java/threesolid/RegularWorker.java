//Created a RegularWorker class to implement actions taken by a human worker. These actions
//are separate than that of a Robot worker. Actions such as eating, resting, and calculating 
//are added in as an Override. This implements the Interface Segregation Princple by separating
//what the RegularWorker is able to do versus what the Robot can do. These minimal actions represent
//the Single Responsibility Principle. While the list of actions are minimal these methods can be
//be extended to incorporate the Open Closed Principle.
package threesolid;

public class RegularWorker implements IWorkable, IFeedable, IRestable, ICalculate {

    @Override
    public String eat() {
        return "I'm eating.";
    }

    @Override
    public String work() {
        return "I'm working.";
    }

    @Override
    public String rest() {
        return "zzzzzz";
    }

    @Override
    public Boolean calculate(int a, int b) {

            if ((a+b) > 0)
                return(true);
            return(false);
    }
}
