/* Assignee: Yrua Riley
    The SuperWorker class follows the single responsibility principle because its only job is to report information
    pertaining to its superworker status. It is also following the interface Segregation principle because it implements
    three interfaces that implement one specific action each.
 */


package threesolid;

public class SuperWorker extends  Worker implements IWorkable, IFeedable, IRestable{
    private static final int restMinutes = 30;

    @Override
    public String work() {
        return "My name is " + getName() + " and I am a super worker!";
    }
    @Override
    public String eat() {
        return "I'm eating a super healthy meal";
    }

    @Override
    public String rest() {
        return "My break is : " + restMinutes + " minutes";
    }
}
