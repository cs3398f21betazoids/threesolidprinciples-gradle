package threesolid;

public class Worker implements IWorkable, IFeedable{
    private String name = "";

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String work()
    {

        if (name == "")
        {
            return "I'm working already!";
        }
        else
        {
            return name + " is working very hard!";
        }
    }

    public String eat()
    {
        if (name == "")
        {
            return "I'm eating already!";
        }
        else
        {
            return name + " is eating a double cheeseburger with special sauce and bacon flavored Skittles!";
        }
    }

    public String rest()
    {
        if (name == "")
        {
            return "Imma take a nap right here. Yeah. K, goodnight";
        }
        else
        {
            return name + " is tucked away sleepin'";
        }
    }
}


