package threesolid;

//Miussett Salomon
//Worked on Robot class and IWorkable interface.
//We have reorganized each class into a separate file.
//We added an additional interface IWorkable because
//once we added the Robot class, the Robot does not need
//to use the eat method.
//This is due to the Interface Segregation Principle.
//We are also splitting the responsibilities through different
//interfaces which supports the Single Responsibility Principle.
//Having a minimal responsibility for each class gives us a
//more flexible design.
//Having methods that can be extended that will cause less bugs
//supports the open closed principle.


public class Robot implements IWorkable {

	@Override
	public String work() {
		return "Robot is working.";
	}

}


