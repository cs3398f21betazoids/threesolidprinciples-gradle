package threesolid;

public interface IFeedable {
    public String eat();
}
