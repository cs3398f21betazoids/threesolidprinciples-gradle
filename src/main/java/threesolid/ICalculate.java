package threesolid;

public interface ICalculate {
    public Boolean calculate(int a, int b);
}
